import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Driver {
	// start 21.41
	public static void modifyWheel(Car obj, int wheelCount)
	/* Summary: method to change wheel count of a given car object 
	 */
	{
		Class objClass =obj.getClass(); // what to with the warning ?
		try {
			Field wheelField = objClass.getDeclaredField("wheelCount");
			wheelField.setAccessible(true);
			wheelField.setInt(obj, wheelCount);
			wheelField.setAccessible(false);

		} catch (Exception e) {
			System.out.println("Something went wrong. Terminating process");
			System.exit(1);
		}

	}

	public static void main(String[] args) throws Exception, SecurityException {
		Car mycar = new Car(12);
		System.out.println("Program started...\nwheel count of mycar is: " + mycar.getWheelCount());
		// start with class then get its fields.
		Class classOfCar = mycar.getClass();
		// get all fields then find which stores wheel count
		// or just getField("wheelCount");
		Field[] fieldsOfCar = classOfCar.getDeclaredFields();
		Field wheelField = classOfCar.getDeclaredField("wheelCount");
		// change wheelCount accessibility
		wheelField.setAccessible(true);
		// wheel 4
		wheelField.setInt(mycar, 4);
		System.out.println("wheel count of mycar is: " + mycar.getWheelCount());
		// wheel 2
		wheelField.setInt(mycar, 2);
		System.out.println("wheel count of mycar is: " + mycar.getWheelCount());
		// wheel 1
		wheelField.setInt(mycar, 145345);
		System.out.println("wheel count of mycar is: " + mycar.getWheelCount());
		wheelField.setAccessible(false);
		// end 22.03
		// methods fln
		Method[] methods = classOfCar.getDeclaredMethods();
		for (Method m : methods) {
			System.out.println("method names: " + m.getName());
		}
		Method m3 = classOfCar.getDeclaredMethod("method3");
		m3.setAccessible(true);
		m3.invoke(mycar);
	}
}
